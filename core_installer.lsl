#ifdef INSTALLER_CORE

debug(string message, key id){
    if(scriptDebug == TRUE){
      llOwnerSay("DebugMSG: \n"+llGetScriptName()+"\n"+message+" \n From: "+llKey2Name(id));
    }
}

starteffects(key id) {
    llParticleSystem(
    [
    // texture parameters:
    PSYS_SRC_TEXTURE , "",
    PSYS_PART_START_SCALE , <0.06,0.19,0.00>, PSYS_PART_END_SCALE , <0.05,0.60,0.00>,
    PSYS_PART_START_COLOR , <0.50,1.00,1.00>, PSYS_PART_END_COLOR , <0.50,1.00,1.00>,
    PSYS_PART_START_ALPHA , (float)1.00, PSYS_PART_END_ALPHA , (float)1.00,
    // production parameters:
    PSYS_SRC_BURST_PART_COUNT , 19,
    PSYS_SRC_BURST_RATE , (float)0.06,
    PSYS_PART_MAX_AGE , (float)3.36,
    PSYS_SRC_MAX_AGE , (float)0.00,
    // placement parameters:
    PSYS_SRC_PATTERN , 4,
    PSYS_SRC_BURST_SPEED_MIN , (float)1.00, PSYS_SRC_BURST_SPEED_MAX , (float)1.00,
    PSYS_SRC_BURST_RADIUS , (float)0.00,
    // placement parameters (for angles only):
    PSYS_SRC_ANGLE_BEGIN , (float)0.00,
    PSYS_SRC_ANGLE_END , (float)0.00,
    PSYS_SRC_OMEGA , <0.00,0.00,0.00>,
    // after-effects & influences:
    PSYS_SRC_ACCEL , <0.00,0.00,0.00>,
    PSYS_SRC_TARGET_KEY , id,
    PSYS_PART_FLAGS , ( 0 // texture & influence options:
       | PSYS_PART_INTERP_COLOR_MASK
       | PSYS_PART_INTERP_SCALE_MASK
    // | PSYS_PART_BOUNCE_MASK
    // | PSYS_PART_WIND_MASK
    // | PSYS_PART_FOLLOW_SRC_MASK
       | PSYS_PART_FOLLOW_VELOCITY_MASK
       | PSYS_PART_TARGET_POS_MASK
       | PSYS_PART_TARGET_LINEAR_MASK
       | PSYS_PART_EMISSIVE_MASK
    )
    ]
    ); 
}

    /*>Functions*/
doMenu( key user, string menuname ) {
    llMessageLinked(llGetLinkNumber(),LM_DOMENU,menuname,user);
}

resetMenu() {
    llMessageLinked(llGetLinkNumber(),10004,"",NULL_KEY); // 10004: LM_RESETMENUSYSTEM
}
/*Menu Engine End*/

strideLists() {
    options = [];
    integer i = llGetListLength(keys);
    integer f = 0;
    while (f<i) {
        options += llKey2Name(llList2Key(keys, f));
        installOpts += "Install|"+llKey2Name(llList2Key(keys, f));
        options += llList2Key(keys, f);
        installOpts += llList2Key(keys, f);
        f++;
    }
    nameParser();
    llOwnerSay((string)options);
  
}

integer checkOwner(key id){
  if (llGetOwner() == id)
  {
    return TRUE;
  } else
  {
    return FALSE;
  }
}

killObject() {
  if (llGetAttached()==0)
  {
    if (scriptDebug==FALSE)
    {
      llDie();
      
    } else
    {
      llOwnerSay("This is normally where I die");
    }
  }

}

init() {
      if (llGetAttached()==0)
      {
        llSensorRepeat("", NULL_KEY, AGENT,30.0, PI, 10.0);  
      }
      llDialog(llGetOwner(), "This installer does not check for duplicates\nPlease make sure that this is the first time you have\ninstalled this product.\n\nThe intaller comes with an auto cleanup feature. It will delete itself after "+(string)((integer)cleanTimer/60)+" Minutes.", ["Ok"], -1);
      llParticleSystem([]);
      if (llGetAttached()==0)
      {
        llSetRot(llEuler2Rot(<0.0,90.0,0.0>));
      }
      options = [];
      options2 = [];
      installOpts = [];
      string packagedGoods = llGetInventoryName(INVENTORY_OBJECT, 0);
      llSay(-652, "install|"+packagedGoods);
      llSetText("", <0,0,0>, 0.0);
      inventory = llGetInventoryNumber(INVENTORY_OBJECT);
      //vector rezPos = llGetPos();
      //llSetRegionPos(rezPos + <0,0,1.5>);
}

init2() {
  string packagedGoods = llGetInventoryName(INVENTORY_OBJECT, 0);
  llSay(-652, "install|"+packagedGoods);
}
string identify(string message) {
    integer length = llStringLength("Install|");
    string result = llGetSubString(llList2String(options, 0), length, -1);
    return result;
    
}

nameParser() {
  options2 = [];
  integer nameCount = llGetListLength(options);
  llOwnerSay((string)nameCount);
  options2=llList2ListStrided(options, 0, -1, 2);
  options2 += "Reset";
}

string parseData(string messageData, integer index) {
    list dataList = llParseString2List(messageData, ["|"], [""]);
    string selection = llList2String(dataList, index);
    return selection;
}
integer identifyHUD(key id) {
  string objectName = llKey2Name(id);
  list separators = llParseString2List(objectName, ["[","]"], [""]);
  if(llListFindList(separators, ["HUD"])>0) {
    return 1;
  } else {
    return 0;
  }
}

theAction() {
  llGiveInventory(reciever, llGetInventoryName(INVENTORY_OBJECT, 0)); 
  llPlaySound(soundUUID, 1.0);
  llSleep(particleTimer);
  llParticleSystem([]);
  llSleep(preDie);
  killObject();
}

#endif
