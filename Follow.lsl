//adding this script as a less laggy and more efficient way of doing the same as above
//this is for havok4's new functions
vector offset = < 1, 0, 1>;  //1 meter behind and 1 meter above owner's center.
integer attached;
default
{
    state_entry()
    {
        
        // Little pause to allow server to make potentially large linked object physical.
        if (llGetAttached()==0)
        {
            llSetStatus(STATUS_PHYSICS, TRUE);
            llSetStatus(STATUS_PHANTOM, TRUE);
            llSleep(0.1);
            llSetTimerEvent(1.0);
        }
    }
    on_rez(integer start_param)
    {
        llSetRot(llEuler2Rot(<0,90,0>));
        
        if (llGetAttached()==0)
        {
            llSetStatus(STATUS_PHYSICS, TRUE);
            llSetStatus(STATUS_PHANTOM, TRUE);
            llSleep(0.1);
            llSetTimerEvent(1.0);
        }
    }
    timer()
    {
        list det = llGetObjectDetails(llGetOwner(),[OBJECT_POS,OBJECT_ROT]);//this will never fail less owner is not in the same sim
        // Owner detected...
        // Get position and rotation
        vector pos   = llList2Vector(det,0);
        rotation rot = (rotation)llList2String(det,1);
        // Offset back one metre in X and up one metre in Z based on world coordinates.
        // use whatever offset you want.
        //vector worldOffset = offset;
        // Offset relative to owner needs a quaternion.
        vector avOffset = offset * rot;
 
        pos += avOffset;       // use the one you want, world or relative to AV.
        if (llGetAttached()==0)
        {
            llMoveToTarget(pos,0.4);
            
        } else {
            
            llSetTimerEvent(0.0);
        }
    }
    attach(key id)
    {
        attached = 1;
    }
}