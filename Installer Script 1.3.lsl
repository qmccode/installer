#define INST_SUP
#include "installer_supplemental.lsl"

integer dupes;
integer present;


default
{
    state_entry()
    {
        dupes = 0;
        llSetText("", <0,0,0>, 0.0);
        llListen(-652, "", "", "");
    }

    touch_start(integer total_number)
    {
        
    }
    on_rez(integer start_param) {
      
      
    }
    changed(integer change) {
          if (change && CHANGED_INVENTORY) {
                llOwnerSay(llGetInventoryName(INVENTORY_OBJECT, 0)+ " Installed");
                
          }
    }
    listen(integer channel, string name, key id, string message) {
      llRegionSay(-653, "Ready");
      string inventory = parseData(message,1);
      string choice = parseData(message,0);
      dupes = detectDupes(inventory);
      if(llGetOwnerKey(id)==llGetOwner()) 
      {
        if(dupes==0)
        { 
          if(choice == "Reset") 
          {
                llResetScript();
          }
        }
      }
    }
}
