The breakdown is this. When the installer egg is loaded, the egg will issue two dialoges. The first is one that will say that there are no objects in the egg, the second is a remider that the installer doesnt check for duplicates and that it will self delete after a set amount of time.

Once the exterior is placed in the installer's inventory, the first of the two notices will not show. it will not appear at all.

The only bug that I am aware of is that if there is more than once installer egg out, it will duplicate install locations. 

The scripts and what they are are as follows:

Follow.lsl - This is all the follow functions, it is separate because I havent integrated it into the main script yet.

Install Script 1.2 - This is the main script with all of the meat and bones. it contains all of the main script functions and is where everything but the follow functions should be edited. At the top the User editable variables can be found. That means anyone who is not a scripter can edit these scripts.

Installer Script 1.3 - This is what is meant to go in the HUD and Pad for recieving the exterior. it helps the Installer egg locate the HUD and Pads for installing.

Menu Engine 2.0 - This Drives all the menu operations in the egg. this does not need to be edited at all.

core_installer.lsl - This has functions which are included by the LSL Preprocessor in Firestorm. 

debug.lsl - This has the debug function, which has a toggle in the user editable variables in the main Install Script.

menudefs - This is a notecard. it has the definitions of the menus and their options. 