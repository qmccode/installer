/*
 *
 The comments follow the following convention:
 >Multi Line Comments like this apply to the code below it. 
 >Single line comments apply to the code to the immediate left.
 *
 These are the user editable variables that can be changed to fit special effects and sound effects.
 *
 */
float preDie = 5; //Time before the object deletes itself after install
float particleTimer = 10; //Time the particle effects last
float cleanTimer = 60; //time in seconds that the installer deletes itself if left alone, pre-install
integer scriptDebug = TRUE; //turn this to TRUE to troubleshoot issues with the script. This can cause things to be spammy 
string soundUUID = ""; //This is not implemented yet, but if you add a sound, use the variable where you use llPlaySound() and then change the UUID of the sound here.


/*
 *
 This portion of the variables controls the "channels" that the dialogs operate on when they are generated, they are used to communicate via link message and identify the type of data that is being sent.
 *
 */
integer LM_DOMENU   = 10001; //Used for calling the menu
integer LM_OPTIONDATA = 10002; //Used for identifying options
/*
 *
 These variables are all for the functions of the installer
 *
 */
key reciever; //The object that is recieving the intallable object
integer rezzed; //True or False Variable for if the installer is rezzed or not.
integer inventory; //integer variable for identifying whether or not the installer has something to install
list installOpts; //List of install locations, this is used in a later function
list keys; //Keys for the objects in the previous list variable
list options; //
list options2;//List of options for choosing an install marker
integer cleanupMarker = 0; // integer variable determining if the installer needs to be cleaned up and to fire off any functions 
                           // related to cleanup
/*
 *
 Name Parser pulls the names of the objects and puts them in a list to be used as a dialog for choosing an install location
 *
 */
nameParser() {
  options2 = [];
  integer nameCount = llGetListLength(options);
  llOwnerSay((string)nameCount);
  options2=llList2ListStrided(options, 0, -1, 2);
  options2 += "Reset";
}

/*
 *
 This creates a list of options to use later when selecting one of multiple install locations
 *
 */

strideLists() {
    options = [];
    integer i = llGetListLength(keys);
    integer f = 0;
    while (f<i) {
        options += llKey2Name(llList2Key(keys, f));
        installOpts += "Install|"+llKey2Name(llList2Key(keys, f));
        options += llList2Key(keys, f);
        installOpts += llList2Key(keys, f);
        f++;
    }
    nameParser();
    llOwnerSay((string)options);
  
}
/*
 *
 This starts the particle effect. This was done this way to allow a change in the target of the particle stream. Putting in the Owner ID if the install location was determined to be a HUD, and to the pad if it was determined to be something other than a HUD.
 *
 */
starteffects(key id) {
    llParticleSystem(
    [
    
    PSYS_SRC_TEXTURE , "",
    PSYS_PART_START_SCALE , <0.06,0.19,0.00>, PSYS_PART_END_SCALE , <0.05,0.60,0.00>,
    PSYS_PART_START_COLOR , <0.50,1.00,1.00>, PSYS_PART_END_COLOR , <0.50,1.00,1.00>,
    PSYS_PART_START_ALPHA , (float)1.00, PSYS_PART_END_ALPHA , (float)1.00,
    
    PSYS_SRC_BURST_PART_COUNT , 19,
    PSYS_SRC_BURST_RATE , (float)0.06,
    PSYS_PART_MAX_AGE , (float)3.36,
    PSYS_SRC_MAX_AGE , (float)0.00,
    
    PSYS_SRC_PATTERN , 4,
    PSYS_SRC_BURST_SPEED_MIN , (float)1.00, PSYS_SRC_BURST_SPEED_MAX , (float)1.00,
    PSYS_SRC_BURST_RADIUS , (float)0.00,
    
    PSYS_SRC_ANGLE_BEGIN , (float)0.00,
    PSYS_SRC_ANGLE_END , (float)0.00,
    PSYS_SRC_OMEGA , <0.00,0.00,0.00>,
    
    PSYS_SRC_ACCEL , <0.00,0.00,0.00>,
    PSYS_SRC_TARGET_KEY , id,
    PSYS_PART_FLAGS , ( 0 
       | PSYS_PART_INTERP_COLOR_MASK
       | PSYS_PART_INTERP_SCALE_MASK
    
    
    
       | PSYS_PART_FOLLOW_VELOCITY_MASK
       | PSYS_PART_TARGET_POS_MASK
       | PSYS_PART_TARGET_LINEAR_MASK
       | PSYS_PART_EMISSIVE_MASK
    )
    ]
    ); 
}
/*
 *
 This resets the menu, it is used if we want to reset the menu system. the Menu Engine 2.0 script has all the core functions related to this.
 *
 */
resetMenu() {
    llMessageLinked(llGetLinkNumber(),10004,"",NULL_KEY); 
}
/*
 *
 This is for processing serialized data by taking a string input formatted a specific way and using an index to select the part of the data we want to use.
 *
 */
string parseData(string messageData, integer index) {
    list dataList = llParseString2List(messageData, ["|"], [""]);
    string selection = llList2String(dataList, index);
    return selection;
}
/*
 *
 What do you think this one does?
 *
 */
killObject() {
  if (llGetAttached()==0)
  {
    if (scriptDebug==FALSE)
    {
      llDie();
      
    } else
    {
      llOwnerSay("This is normally where I die");
    }
  }

}
/*
 *
 This is all the stuff we want to run when the object is rezzed, or attached, and even if the object is reset. anything that needs to be done to prepare to run the script can be put here.
 *
 */
init() {
      if (llGetAttached()==0)
      {
        llSensorRepeat("", NULL_KEY, AGENT,30.0, PI, 10.0);  
      }
      llDialog(llGetOwner(), "This installer does not check for duplicates\nPlease make sure that this is the first time you have\ninstalled this product.\n\nThe intaller comes with an auto cleanup feature. It will delete itself after "+(string)((integer)cleanTimer/60)+" Minutes.", ["Ok"], -1);
      llParticleSystem([]);
      if (llGetAttached()==0)
      {
        llSetRot(llEuler2Rot(<0.0,90.0,0.0>));
      }
      options = [];
      options2 = [];
      installOpts = [];
      inventory = llGetInventoryNumber(INVENTORY_OBJECT);
      
      
}
/*
 *
 This is designed to identify the HUD, which would allow the installer to direct the particle stream to the owner instead of it'self, since attached objects cannot be the target of a particle effect.
 *
 */
integer identifyHUD(key id) {
  string objectName = llKey2Name(id);
  list separators = llParseString2List(objectName, ["[","]"], [""]);
  if(llListFindList(separators, ["HUD"])>0) {
    return 1;
  } else {
    return 0;
  }
}
/*
 *
 This Function initiates a menu according to the menu engine. further documentation can be found in the notecard about it
 *
 */
doMenu( key user, string menuname ) {
    llMessageLinked(llGetLinkNumber(),LM_DOMENU,menuname,user);
}
/*
 *
 This is the debug function, designed to output useful additional information about what is going on inside the script that would help with finding occasions where the script is not functioning properly
 *
 */
debug(string message, key id){
    if(scriptDebug == TRUE){
      llOwnerSay("DebugMSG: \n"+llGetScriptName()+"\n"+message+" \n From: "+llKey2Name(id));
    }
}



default
{
    state_entry()
    {
      debug("State Entry", NULL_KEY);
      rezzed = 1;
      if (inventory>0 && llGetAttached()==0) 
      {
        if (scriptDebug == FALSE)
        {
          llOwnerSay("Timer Started");            
          llSetTimerEvent(cleanTimer);
        } 
      } else 
      {
            llDialog(llGetOwner(), "Load The Product and take the installer", ["Ok", "Reset"], -653);            
      }
      llListen(-653, "", "", "");
      init();
    }
    on_rez(integer start_param) {
      debug("On Rez", NULL_KEY);
      init();
      inventory = llGetInventoryNumber(INVENTORY_OBJECT);
      if (inventory>0 && llGetAttached()==0) 
      {
        if (scriptDebug ==FALSE)
        {
          llOwnerSay("Timer Started");            
          llSetTimerEvent(cleanTimer);
        } 
      } else 
      {
            llDialog(llGetOwner(), "Load The Product and take the installer", ["Ok"], -125);            
      }
    }
    touch_start(integer total_number)
    {
      debug("Touch Start", llDetectedKey(0));
      if(llGetInventoryNumber(INVENTORY_OBJECT)==0){
        llDialog(llGetOwner(), "Please Add Object to install.", ["Ok"], -999);
      } else {
        if (llGetListLength(keys)>0)
        {
            if (llGetListLength(keys)>1) {
              strideLists();
              llDialog(llGetOwner(), "Pick an Install Location", options2, -653);
              
            } else {
              reciever = llList2Key(keys, 0);
              doMenu(llGetOwner(), "DEFAULT");
              
            }
        } else
        {
            llOwnerSay("No Place to Install");
            doMenu(llGetOwner(),"EMPTY");
        }
            
      }
        
    }
    
    sensor(integer num_detected)
    {
      if (llDetectedKey(0)!=llGetOwner())
       {
         llShout(0, "Owner Has Gone, Starting Auto Cleanup Timer 3 Minutes Remaining.");
         if (scriptDebug==FALSE)
           {
            cleanupMarker = 1;
            llSetTimerEvent(cleanTimer);
           }  
       } 
    }
    listen(integer channel, string name, key id, string message) 
    {
      debug(message,id);
        if (message == "Ready" && llGetOwnerKey(id)==llGetOwner()) 
        {
          debug("Collecting Keys",NULL_KEY);
            keys += id;
        } else if(llListFindList(options2, [message])>0)
        {
          debug("Installing Functions",NULL_KEY);
            reciever = llList2Key(options,llListFindList(options, [message])+1);
            
            if(identifyHUD(reciever)==0) 
            {
              starteffects(reciever);
              if (soundUUID)
              {
                  llPlaySound(soundUUID, particleTimer);
              }
              llSleep(particleTimer);
              llParticleSystem([]);
              llSleep(preDie);
              killObject();
            } else 
            {
              starteffects(llGetOwner());
              if (soundUUID)
              {
                  llPlaySound(soundUUID, particleTimer);
              }              
              llSleep(particleTimer);
              llParticleSystem([]);
              llSleep(preDie);
              killObject();
            }
        } else {
          
        }
    }
    link_message(integer sender_num, integer num, string str, key id) 
    {
      debug("link Message of '"+str+"'",id);
            if (num == LM_OPTIONDATA) 
            {
              str = parseData(str,1);
                  if (str == "Install") 
                  {
                      
                      if(identifyHUD(reciever)>!0) 
                      {
                        starteffects(reciever);
                        if (soundUUID)
                        {
                            llPlaySound(soundUUID, particleTimer);
                        }
                        llSleep(particleTimer);
                        llParticleSystem([]);
                        llSleep(preDie);
                        killObject();
                      } else 
                      {
                        starteffects(llGetOwner());
                        llGiveInventory(reciever, llGetInventoryName(INVENTORY_OBJECT, 0)); 
                        if (soundUUID)
                        {
                            llPlaySound(soundUUID, particleTimer);
                        }
                        llSleep(particleTimer);
                        llParticleSystem([]);
                        llSleep(preDie);
                        killObject();
                      }
                  } else if(str == "Reset") 
                  {
                    debug("Script Resetting",NULL_KEY);
                    llRegionSay(-652, "Reset");
                    resetMenu();
                    llResetScript();
                  }
            } 
    }
    timer() 
    {
      debug("Timer",NULL_KEY);
      if (rezzed == 1 && inventory) 
      {
        if (scriptDebug==FALSE)
        {
          llDie();
        } else {
          debug("URK I DIE (Debug is on)",NULL_KEY);
        }
            
      } else if (cleanupMarker == 1 && scriptDebug == FALSE)
      {
        llInstantMessage(llGetOwner(), llGetObjectName()+" Is Cleaning Up using Auto Clean.");
        llDie();
      } else if (scriptDebug == TRUE)
      {
        llInstantMessage(llGetOwner(), llGetObjectName()+" Is Cleaning Up using Auto Clean.(DEBUG)");
        llSetTimerEvent(0.0);
        
      }
    }
    changed(integer change)
    {
      debug("Chage Event",NULL_KEY);
      if (change && CHANGED_INVENTORY)
      {
        llResetScript();
      }
    }
}

