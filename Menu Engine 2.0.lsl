integer numMenus;
integer nList;
list menudata;
list menuOffsets;
integer isListening = FALSE;
key cqid;
string confNoteName = "menudefs";
string clMenuText;
string clMenuID;
list clButtons;
list clActions;
key cUser;
integer cSenderID;
integer cOfs;
integer cMenuType;
integer cMenuNum;
string cMenuName;
integer cLine;
list cButtons;
list cActions;
float RESPONSE_TIMEOUT = 80.0;
integer MT_READSTRING = 3;
integer MT_NORMAL = 1;
integer MT_ASK = 2;
integer MENUREC_SIZE = 4;
integer MC_TOMENU = 2;
integer MC_OPTIONASK = 4;
integer MC_OPTION = 3;
integer LM_RESETMENUSYSTEM = 10004;
integer LM_READSTRINGDATA = 10006;
integer LM_READSTRING = 10005;
integer LM_OPTIONDATA = 10002;
integer LM_MENULOADED = 10003;
integer LM_DOMENU   = 10001;
integer CHATBASECHANNEL = 100050;

emitMsgGlobal( integer num ) {
    llMessageLinked(-1,num,"",NULL_KEY);
}





configLoaded()
{
    llSay(0, "Menu Initialized, mem: "+(string)llGetFreeMemory()+". "+llGetObjectName()+" Ready");
    emitMsgGlobal(LM_MENULOADED);
}


commitEntry() {

    integer sofs = llGetListLength(menudata);
    menuOffsets += [ clMenuID, sofs, sofs + 1 + llGetListLength(clButtons), sofs + 1 + llGetListLength(clButtons) + llGetListLength(clActions) ];
    menudata += [ clMenuText ];
    menudata += clButtons;
    menudata += clActions;

    clMenuID = "";
    clMenuText = "";
    clButtons = [];
    clActions = [];
}



startReadConfig() {
    numMenus = 0;
    menudata = [];
    menuOffsets = [];
    clMenuID = "";
    clButtons = [];
    clActions = [];
    clMenuText = "";

    cLine = 1;
    cqid = llGetNotecardLine( confNoteName, cLine - 1 );
}


readConfigLine( string data ) {
    if (data != EOF) {
        integer spos = llSubStringIndex(data," ");
        if (spos==-1) jump LreadNextLine;

        string cmd = llGetSubString( data, 0, spos - 1);
        data = llGetSubString( data, spos+1, -1);

        integer cmdid = llListFindList(["MENU","TEXT","TOMENU","OPTION","OPTIONASK"],[cmd]);
        if (cmdid==-1) jump LreadNextLine;

        if (cmdid==0) {
            if (clMenuID!="") commitEntry();
            clMenuID = data;
        }
        else if (cmdid==1) {
            list tmpl = llParseString2List(data,["\\n"],[]);
            data = llDumpList2String(tmpl,"\n");
            clMenuText += data;
        }
        else if (cmdid==2) {
            integer wpos = llSubStringIndex(data," ");
            string tomenuid = data;
            string btext = data;
            if (wpos>=0) {
                tomenuid = llGetSubString(data,0,wpos - 1);
                btext = llGetSubString(data,wpos+1,-1);
            }

            clButtons  += [ btext ];
            clActions  += [ MC_TOMENU, tomenuid ];
        }
        else if (cmdid==3) {
            clButtons  += [ data ];
            clActions  += [ MC_OPTION, data ];
        }
        else if (cmdid==4) {
            clButtons  += [ data ];
            clActions  += [ MC_OPTIONASK, data ];
        }

        @LreadNextLine;
        cLine++;
        cqid = llGetNotecardLine( confNoteName, cLine - 1 );
    } else {
            numMenus = llGetListLength(menuOffsets) / MENUREC_SIZE;
        configLoaded();
    }
}
integer getMenuNum( string menuid ) {
    integer c;
    for (c=0;c<numMenus;c++)
        if (llList2String(menuOffsets,c*MENUREC_SIZE) == menuid)
            return(c);
    return(-1);
}


emitMsgSender( integer num, string msg ) {
    llMessageLinked(-1,num,msg,cUser);
}





doMenu( key user,  string menuname ) {
    integer mnum = getMenuNum(menuname);
    if ( mnum == -1 ) {
        llSay(0,"Please wait for: "+menuname+" menu to load.");
        return;
    }

    cOfs = llList2Integer( menuOffsets, mnum * MENUREC_SIZE + 1 );
    integer actOfs = llList2Integer( menuOffsets, mnum * MENUREC_SIZE + 2 );
    integer actEnd = llList2Integer( menuOffsets, mnum * MENUREC_SIZE + 3 );

    cMenuNum = mnum;
    cMenuName = menuname;
    cUser = user;
    cButtons = llList2List( menudata, cOfs+1, actOfs - 1 );
    cActions = llList2List( menudata, actOfs, actEnd );
    cMenuType = MT_NORMAL;

    clearListens();
    nList = llListen( CHATBASECHANNEL + cMenuNum, "", user, "");
    llDialog( user, llList2String( menudata, cOfs ), cButtons, CHATBASECHANNEL + cMenuNum );

    isListening = TRUE;
    llSetTimerEvent(RESPONSE_TIMEOUT);
}




doAskMenu( key user, string param ) {
    cMenuType = MT_ASK;
    cButtons = [ "Yes", "No" ];
    cActions = [ param ];
    llDialog( user, param, cButtons, CHATBASECHANNEL + cMenuNum );
    clearListens();
    nList = llListen( CHATBASECHANNEL + cMenuNum, "", user, "");
    isListening = TRUE;
    llSetTimerEvent(RESPONSE_TIMEOUT);
}


clearListens() {
    if (isListening) {
        llListenRemove(nList);
        isListening = FALSE;
    }
}



default
{
    state_entry()
    {
        llSay(0, "Loading menu... mem: "+(string)llGetFreeMemory());
        startReadConfig();
    }

    dataserver(key query_id, string data) {
        if (query_id==cqid) readConfigLine(data);
    }

    listen( integer channel, string name, key id, string message ) {
        llSetTimerEvent(0.0);
        clearListens();

        if (cMenuType==MT_ASK) {
            if (message=="Yes") {
                string param = llList2String( cActions, 0 );
                emitMsgSender(LM_OPTIONDATA,llDumpList2String([ cMenuName, param ],"|"));
            }
            return;
        }
        if (cMenuType==MT_READSTRING) {
            string param = llList2String( cActions, 0 );
            emitMsgSender(LM_READSTRINGDATA,llDumpList2String([ param, message ],"|"));
            return;
        }

        integer c;
        integer nb = llGetListLength(cButtons);
        integer fnd = -1;
        for (c=0;c<nb;c++) {
            if (llList2String(cButtons,c)==message) {
                fnd = c;
                jump doneSR;
            }
        }
        @doneSR;
        if (fnd>=0) {
            integer act = llList2Integer( cActions, fnd * 2 );
            string param = llList2String( cActions, fnd * 2 + 1 );
            if (act==MC_TOMENU) {
                string tomenu = param;
                integer spos = llSubStringIndex(tomenu," ");
                if (spos>=0) tomenu = llGetSubString(tomenu,0,spos - 1);

                doMenu(id,tomenu);
                return;
            }
            if (act==MC_OPTION) {
                emitMsgSender(LM_OPTIONDATA,llDumpList2String([ cMenuName, param ],"|"));
                return;
            }
            if (act==MC_OPTIONASK) {
                doAskMenu(id,param);
                return;
            }

        }
    }

    link_message(integer sender_num, integer num, string str, key id) {
        if (num==LM_DOMENU) {
            cSenderID = sender_num;
            doMenu(id,str);
            return;
        }
        if (num==LM_READSTRING) {
            cSenderID = sender_num;
            cMenuType = MT_READSTRING;

            string message = str;
            string retval  = str;
            integer spos = llSubStringIndex(str," ");
            if (spos>=0) {
                message = llGetSubString( str, spos+1, -1 );
                retval = llGetSubString( str, 0, spos - 1 );
            }
            cActions = [ retval ];
            nList = llListen( 0, "", id, "");
            isListening = TRUE;
            llSay(0,message);
            llSetTimerEvent(RESPONSE_TIMEOUT);
            return;
        }
        if (num==LM_RESETMENUSYSTEM) {
            llOwnerSay("resetting script - " + str);
            llResetScript();
        }
    }

    timer() {
        llSetTimerEvent(0.0);
        clearListens();
    }
    //For Maintenance uncomment this section
    // changed(integer change)
    //{
    //    if(change & CHANGED_INVENTORY) llResetScript();
    //}
}
